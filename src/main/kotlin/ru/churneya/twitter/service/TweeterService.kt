package ru.churneya.twitter.service

interface TweeterService {
    fun getTweetsBySearch(word: String)
    fun getTweetsByHashtag(hashtag: String)
    fun getTweetsByLogin(login: String)
}
