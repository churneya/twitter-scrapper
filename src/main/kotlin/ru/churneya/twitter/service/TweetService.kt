package ru.churneya.twitter.service

import org.openqa.selenium.WebElement
import ru.churneya.twitter.model.dto.TweetDto
import ru.churneya.twitter.model.entity.TweetEntity

interface TweetService {
    fun getTweedData(card: WebElement): TweetDto
    fun saveTweet(tweet: TweetEntity)
}
