package ru.churneya.twitter.service.impl

import org.openqa.selenium.By
import org.openqa.selenium.Cookie
import org.openqa.selenium.Keys
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.chrome.ChromeDriver
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Service
import ru.churneya.twitter.config.TwitterAuthProperties
import ru.churneya.twitter.model.dto.TweetDto
import ru.churneya.twitter.service.TweetService
import ru.churneya.twitter.service.TweeterService
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.ZonedDateTime
import java.util.Date
import java.util.StringTokenizer

@Service
@EnableConfigurationProperties(TwitterAuthProperties::class)
class TweeterServiceImpl(
    private val twitterProp: TwitterAuthProperties,
    private val tweetService: TweetService,
    @Qualifier("twitterWebDriver")
    private val driver: ChromeDriver
) : TweeterService {

    val pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"

    override fun getTweetsBySearch(word: String) {
        val driver = ChromeDriver()

        // navigate to login screen
        driver.get("https://twitter.com/search")
        driver.manage().window().maximize()
        sleep(5)

        // find search input and search for term
        val searchInput = driver.findElement(By.xpath("//input[@aria-label=\"Поисковый запрос\"]"))
        searchInput.sendKeys(word)
        searchInput.sendKeys(Keys.RETURN)
        sleep(1)

        // navigate to historical 'latest' tab
        driver.findElement(By.linkText("Последнее")).click()

        sleep(1)

        val tweetsRaw = driver.findElements(By.xpath("//article[@data-testid=\"tweet\"]"))

        tweetsRaw.map {
            val tweet = tweetService.getTweedData(it)
            println(tweet)
        }
        println(tweetsRaw.size)

        driver.close()
    }

    override fun getTweetsByHashtag(hashtag: String) {

        // navigate to login screen
        driver.manage().window().maximize()

        val needToLogin = true

        if (needToLogin) {
            driver.get("https://twitter.com")
            sleep(1)
            loadCookie()
            driver.navigate().refresh()
            sleep(2)
            try {
                driver.findElement(By.xpath("//a[@href='/login' and @role='link']"))
                login()
            } catch (ex: NoSuchElementException) {
                println(ex.localizedMessage)
            }
        }
        val url = "https://twitter.com/hashtag/$hashtag"

        driver.get(url)
        sleep(2)

        // driver.findElement(By.linkText("Последнее")).click()
        // sleep(2)

        var lastPosition = driver.executeScript("return window.pageYOffset;")
        var scrolling = true
        val tweetsData = mutableListOf<TweetDto>()
        val tweetsIds = mutableListOf<Int>()

        while (scrolling) {
            val tweetsRaw = driver.findElements(By.xpath("//article[@data-testid=\"tweet\"]"))
            var dateTime: ZonedDateTime = ZonedDateTime.now()

            tweetsRaw.map {
                try {
                    val tweet = tweetService.getTweedData(it)
                    val tweetId = tweet.hashCode()
                    if (!tweetsIds.contains(tweetId)) {
                        tweetsIds.add(tweetId)
                        tweetsData.add(tweet)
                    }
                    dateTime = tweet.date
                } catch (ex: NoSuchElementException) {
                    println("maybe advert - " + ex.localizedMessage)
                }
            }
            // if(dateTime <= ZonedDateTime.of(2022,1,20,10,34,0,0, ZoneId.of("Europe/Moscow"))){
            //     break
            // }
            if (tweetsData.size > 10) {
                break
            }
            var scrollAttempt = 0

            while (true) {
                driver.executeScript("window.scrollTo(0, document.body.scrollHeight);")
                sleep(2)
                val currentPosition = driver.executeScript("return window.pageYOffset;")
                if (lastPosition == currentPosition) {
                    scrollAttempt++
                    if (scrollAttempt >= 3) {
                        scrolling = false
                        break
                    } else {
                        sleep(2)
                    }
                } else {
                    lastPosition = currentPosition
                    break
                }
            }
        }

        tweetsData.map {
            try {
                tweetService.saveTweet(it.toEntity())
            } catch (ex: Exception) {
                println(it)
            }
        }
        println(tweetsData.size)

        driver.quit()
    }

    override fun getTweetsByLogin(login: String) {
        TODO("Not yet implemented")
    }

    fun login() {
        driver.get("https://twitter.com/i/flow/login")
        sleep(2)

        val loginInput = driver.findElement(By.xpath("//input[@name=\"text\"]"))
        loginInput.sendKeys(twitterProp.username)
        loginInput.sendKeys(Keys.RETURN)
        sleep(2)

        println(driver)

        if (driver.findElement(By.xpath("//span[contains(.,\"Введите номер телефона или имя пользователя\")]")).isDisplayed) {
            val confirmInput = driver.findElement(By.xpath("//input[@name=\"text\"]"))
            confirmInput.sendKeys(twitterProp.handler)
            confirmInput.sendKeys(Keys.RETURN)
            sleep(2)
        }

        val passwordInput = driver.findElement(By.xpath("//input[@name=\"password\"]"))
        passwordInput.sendKeys(twitterProp.password)
        passwordInput.sendKeys(Keys.RETURN)
        sleep(5)
        saveCookie()
    }

    fun saveCookie(file: File = File("build/Cookies.data")) {
        try {
            // Delete old file if exists
            file.delete()
            file.createNewFile()
            val fileWrite = FileWriter(file)
            val bufferedWriter = BufferedWriter(fileWrite)

            // loop for getting the cookie information
            for (ck in driver.manage().cookies) {
                if (ck.expiry != null) {
                    val dateFormat: DateFormat = SimpleDateFormat(pattern)
                    bufferedWriter.write(
                        ck.name.toString() + ";" +
                            ck.value + ";" +
                            ck.domain + ";" +
                            ck.path + ";" +
                            dateFormat.format(ck.expiry) + ";" +
                            ck.isSecure
                    )
                    bufferedWriter.newLine()
                }
            }
            bufferedWriter.close()
            fileWrite.close()
        } catch (ex: java.lang.Exception) {
            driver.quit()
            ex.printStackTrace()
        }
    }

    fun loadCookie(file: File = File("build/Cookies.data")) {
        try {
            if (!file.exists()) {
                file.parentFile.mkdirs()
                file.createNewFile()
            }
            val fileReader = FileReader(file)
            val bufferedReader = BufferedReader(fileReader)
            var storyline: String?
            while (bufferedReader.readLine().also { storyline = it } != null) {
                val token = StringTokenizer(storyline, ";")
                while (token.hasMoreTokens()) {
                    val name = token.nextToken()
                    val value = token.nextToken()
                    val domain = token.nextToken()
                    val path = token.nextToken()
                    var expiry: Date? = null
                    var dateString: String?
                    if (token.nextToken().also { dateString = it } != "null") {
                        val dateFormat: DateFormat = SimpleDateFormat(pattern)
                        expiry = dateFormat.parse(dateString)
                    }
                    val isSecure = token.nextToken().toBoolean()
                    val ck = Cookie(name, value, domain, path, expiry, isSecure)
                    println(ck)
                    // add the stored cookie to your current session
                    driver.manage().addCookie(ck)
                }
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    fun sleep(seconds: Int) {
        Thread.sleep((seconds * 1000).toLong())
    }
}
