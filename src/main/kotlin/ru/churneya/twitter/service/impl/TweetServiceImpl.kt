package ru.churneya.twitter.service.impl

import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.springframework.stereotype.Service
import ru.churneya.twitter.model.dto.TweetDto
import ru.churneya.twitter.model.entity.TweetEntity
import ru.churneya.twitter.repository.TweetRepository
import ru.churneya.twitter.service.TweetService
import java.time.ZonedDateTime

@Service
class TweetServiceImpl(
    private val twitterRepository: TweetRepository
) : TweetService {
    override fun saveTweet(tweet: TweetEntity) {
        if (twitterRepository.findByHandleAndDate(tweet.handle, tweet.date).isEmpty()) {
            twitterRepository.saveAndFlush(tweet)
        }
    }

    override fun getTweedData(card: WebElement): TweetDto {

        val username = card.findElement(By.xpath(".//span")).text
        // username = Regex("[\\p{InCombiningDiacriticalMarks}]").replace(username, "")

        val handle = card.findElement(By.xpath(".//span[contains(text(), \"@\")]")).text

        val tweetDate = ZonedDateTime.parse(card.findElement(By.xpath(".//time")).getAttribute("datetime"))

        val text = card.findElement(By.xpath(".//div[2]/div[2]/div[2]/div")).text
        val link = card.findElement(By.xpath(".//time/parent::a")).getAttribute("href")
        val replyCount = card.findElement(By.xpath(".//div[@data-testid=\"reply\"]")).text
        val retweetCount = card.findElement(By.xpath(".//div[@data-testid=\"retweet\"]")).text
        val likeCount = card.findElement(By.xpath(".//div[@data-testid=\"like\"]")).text
        val imagesRaw = card.findElements(By.xpath(".//img[@alt=\"Изображение\"]"))
        val videoRaw = card.findElements(By.xpath(".//video"))

        val images = mutableListOf<String>()
        val videos = mutableListOf<String>()

        imagesRaw.map {
            val src = it.getAttribute("src")
            images.add(src)
        }
        videoRaw.map {
            val src = it.getAttribute("src")
            videos.add(src)
        }

        return TweetDto(
            username = username,
            handle = handle,
            date = tweetDate,
            text = text,
            link = link,
            replyCount = replyCount,
            retweetCount = retweetCount,
            likeCount = likeCount,
            images = images
        )
    }
}
