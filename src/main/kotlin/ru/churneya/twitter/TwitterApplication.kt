package ru.churneya.twitter

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import ru.churneya.twitter.service.TweeterService

@SpringBootApplication
class TwitterApplication(
    private val tweeterService: TweeterService
) {

    @Bean
    fun init() {
        tweeterService.getTweetsByHashtag("нюдсочетверг")
    }
}

fun main(args: Array<String>) {
    runApplication<TwitterApplication>(*args)
}
