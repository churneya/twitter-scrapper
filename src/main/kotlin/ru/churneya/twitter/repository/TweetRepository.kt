package ru.churneya.twitter.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.churneya.twitter.model.entity.TweetEntity
import java.time.ZonedDateTime

interface TweetRepository : JpaRepository<TweetEntity, Int> {
    fun findByHandleAndDate(handle: String, Date: ZonedDateTime): List<TweetEntity>
}
