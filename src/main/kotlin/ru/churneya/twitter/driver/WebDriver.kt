package ru.churneya.twitter.driver

import io.github.bonigarcia.wdm.WebDriverManager
import org.openqa.selenium.chrome.ChromeDriver
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class WebDriver {

    @Bean
    fun twitterWebDriver(): ChromeDriver {
        WebDriverManager.chromedriver().setup()
        return ChromeDriver()
    }
}
