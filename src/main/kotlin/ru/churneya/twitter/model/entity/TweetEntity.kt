package ru.churneya.twitter.model.entity

import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import java.time.ZonedDateTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Table(name = "tweets")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType::class)
class TweetEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0,
    val username: String?,
    val handle: String,
    val date: ZonedDateTime,
    val text: String?,
    val link: String,
    val replyCount: String?,
    val retweetCount: String?,
    val likeCount: String?,

    @Type(type = "jsonb")
    var images: List<String>? = mutableListOf()
)
