package ru.churneya.twitter.model.dto

import ru.churneya.twitter.model.entity.TweetEntity
import java.time.ZonedDateTime

data class TweetDto(
    val username: String?,
    val handle: String,
    val date: ZonedDateTime,
    val text: String?,
    val link: String,
    val replyCount: String?,
    val retweetCount: String?,
    val likeCount: String?,
    val images: List<String>?
) {
    fun toEntity(): TweetEntity {
        return TweetEntity(
            username = this.username,
            handle = this.handle,
            date = this.date,
            text = this.text,
            link = this.link,
            replyCount = this.replyCount,
            retweetCount = this.retweetCount,
            likeCount = this.likeCount,
            images = this.images
        )
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TweetDto

        if (username != other.username) return false
        if (handle != other.handle) return false
        if (date != other.date) return false
        if (text != other.text) return false

        return true
    }

    override fun hashCode(): Int {
        var result = handle.hashCode()
        result = 31 * result + date.hashCode()
        result = 31 * result + text.hashCode()
        return result
    }
}
