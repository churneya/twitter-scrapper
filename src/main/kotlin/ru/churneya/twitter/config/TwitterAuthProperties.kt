package ru.churneya.twitter.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "twitter.auth")
data class TwitterAuthProperties(
    val username: String = "",
    val handler: String = "",
    val password: String = ""
)
